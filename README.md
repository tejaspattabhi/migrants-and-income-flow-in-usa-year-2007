# Search based data representation of state-to-state migration flow and its effects based on on gross income.

Team Members: Tejas Tovinkere Pattabhi, Mitsu Deshpande

Project Coded in HTML/CSS, JavaScript and SPARQL


The Project is hosted in the following link: 
`http://www.utdallas.edu/~txp130630/home.html`

The Queries can be found on the following links:
```
1. Query 1: http://www.utdallas.edu/~txp130630/Query1.html
2. Query 2: http://www.utdallas.edu/~txp130630/Query2.html
3. Query 3: http://www.utdallas.edu/~txp130630/Query3.html
```